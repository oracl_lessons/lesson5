create table sequnce_baza(id int,baza_xodimlari varchar2(20) not null primary key,baza_ishlab_chiqilgan_yil date)
;

create sequence SQ_sequnce_baza_TBL
minvalue 1
maxvalue 1000
start with 1
increment by 1;

insert into sequnce_baza(id,baza_xodimlari,baza_ishlab_chiqilgan_yil)values(
       SQ_sequnce_baza_TBL.Nextval,'bosh mutaxassis',to_date('2017.08.10','YYYY-MM-DD'));

insert into sequnce_baza(id,baza_xodimlari,baza_ishlab_chiqilgan_yil)values(
       SQ_sequnce_baza_TBL.Nextval,'amaliyotchi',to_date(sysdate,'YYYY.MM.DD.hh24:mi:ss'));   


select *from sequnce_baza